# generated by cabal-rpm-0.12.1
# https://fedoraproject.org/wiki/Packaging:Haskell

%global pkg_name hxt-regex-xmlschema
%global pkgver %{pkg_name}-%{version}

%bcond_with tests

Name:           ghc-%{pkg_name}
Version:        9.2.0.3
Release:        4%{?dist}
Summary:        A regular expression library for W3C XML Schema regular expressions

License:        MIT
Url:            https://hackage.haskell.org/package/%{pkg_name}
Source0:        https://hackage.haskell.org/package/%{pkgver}/%{pkgver}.tar.gz

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  ghc-bytestring-devel
BuildRequires:  ghc-hxt-charproperties-devel
BuildRequires:  ghc-parsec-devel
BuildRequires:  ghc-text-devel
%if %{with tests}
BuildRequires:  ghc-HUnit-devel
%endif
# End cabal-rpm deps

%description
This library supports full W3C XML Schema regular expressions inclusive all
Unicode character sets and blocks. The complete grammar can be found under
<http://www.w3.org/TR/xmlschema11-2/#regexs>. It is implemented by the
technique of derivations of regular expressions.

The W3C syntax is extended to support not only union of regular sets, but also
intersection, set difference, exor. Matching of subexpressions is also
supported.

The library can be used for constricting lightweight scanners and tokenizers.
It is a standalone library, no external regex libraries are used.

Extensions in 9.2: The library does nor only support String's, but also
ByteString's and Text in strict and lazy variants.


%package devel
Summary:        Haskell %{pkg_name} library development files
Provides:       %{name}-static = %{version}-%{release}
Provides:       %{name}-doc = %{version}-%{release}
%if %{defined ghc_version}
Requires:       ghc-compiler = %{ghc_version}
Requires(post): ghc-compiler = %{ghc_version}
Requires(postun): ghc-compiler = %{ghc_version}
%endif
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package provides the Haskell %{pkg_name} library development
files.


%prep
%setup -q -n %{pkgver}


%build
%ghc_lib_build


%install
%ghc_lib_install


%check
%cabal_test


%post devel
%ghc_pkg_recache


%postun devel
%ghc_pkg_recache


%files -f %{name}.files
%license LICENSE


%files devel -f %{name}-devel.files
%doc examples


%changelog
* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 9.2.0.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Fri Jan 26 2018 Jens Petersen <petersen@redhat.com> - 9.2.0.3-3
- rebuild

* Wed Oct  4 2017 Jens Petersen <petersen@redhat.com> - 9.2.0.3-2
- disable tests due to hanging
  https://github.com/UweSchmidt/hxt/issues/72

* Sat Sep 16 2017 Fedora Haskell SIG <haskell@lists.fedoraproject.org> - 9.2.0.3-1
- spec file generated by cabal-rpm-0.11.2
